console.log('page load - entered main.js for js-other api');

var submitButtonGame = document.getElementById('game-submit-button');
submitButtonGame.onmouseup = getGameInfo;

function getGameInfo(){
    console.log('entered get game info');
    var number = document.getElementById("game-text").value;
    makeNetworkCallToGameApi(number);
} // end of getFormInfo

function makeNetworkCallToGameApi(number){
    console.log('entered makeNetworkCallToGameApi');
    var url = "http://student12.cse.nd.edu:51028/games/" + number;
    var xhr = new XMLHttpRequest(); // 1. creating req
    xhr.open("GET", url, true); // 2. configure request attributes

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        // do something
        updateGameWithResponse(number, xhr.responseText);
    } // end of onload

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    } // end of onerror

    xhr.send(null); // actually send req with no message body
} // end of makeNetworkCallToAgeApi

function updateGameWithResponse(number, response_text){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var label1 = document.getElementById('response-line1');

    if(response_json['result'] == "success"){
        label1.innerHTML = 'Game: ' + response_json['title'] + '<br>Genre: ' + response_json['genre'] + '<br>Platform: ' + response_json['platform'] + '<br>Metascore: ' + response_json['metascore'];
        var rating = response_json['metascore'];
        constructGraphApiUrl(rating);
    } else{
        label1.innerHTML = 'Sorry game not found.';
    }

}

// The only api that I could find that made sense with my video game api from HW08
// was a graph API however, it is not really RESTful and it just uses the query
// string in the url thus it is a bit unlike the example, I hope it is still acceptable :)

function constructGraphApiUrl(rating){
    console.log('entered contstructGraphApiUrl');
    
    var label2 = document.getElementById('response-line2');

    if(rating <= 33){
        var color = "red";
    } else if(rating > 33 && rating <= 66){
        var color = "yellow";
    } else if(rating > 66){
        var color = "green";
    }

    var url = "https://quickchart.io/chart?c={type:'radialGauge',data:{datasets:[{data:[" + rating + "],backgroundColor:'" + color + "'}]}}";

    if(document.getElementsByTagName("img").length > 0){
        document.getElementsByTagName("img")[0].src = url;
    } else {
        var img = document.createElement("img");
        img.src = url;
        label2.appendChild(img);
    }
    console.log(url);
} 

